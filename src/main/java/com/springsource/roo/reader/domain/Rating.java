package com.springsource.roo.reader.domain;

import javax.persistence.ManyToOne;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Rating {

    @ManyToOne
    private Book book;

    @ManyToOne
    private Reader reader;

    @ManyToOne
    private RatingScale ratingscale;
}
