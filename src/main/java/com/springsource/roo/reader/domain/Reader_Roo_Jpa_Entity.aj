// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.roo.reader.domain;

import com.springsource.roo.reader.domain.Reader;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect Reader_Roo_Jpa_Entity {
    
    declare @type: Reader: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Reader.id;
    
    @Version
    @Column(name = "version")
    private Integer Reader.version;
    
    public Long Reader.getId() {
        return this.id;
    }
    
    public void Reader.setId(Long id) {
        this.id = id;
    }
    
    public Integer Reader.getVersion() {
        return this.version;
    }
    
    public void Reader.setVersion(Integer version) {
        this.version = version;
    }
    
}
