package com.springsource.roo.reader.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Book {

    @NotNull
    @Size(max = 150)
    private String title;

    @NotNull
    private Integer PublishYear;

    @ManyToOne
    private Publisher publisher;

    @ManyToOne
    private Category category;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Author> authors = new HashSet<Author>();

    @NotNull
    @Size(max = 150)
    private String tag;

    @Size(max = 500)
    private String description;

    @Size(min = 13, max = 13)
    private String isbn;

    private Integer edition;

    private Integer totalpages;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Rating> rates = new HashSet<Rating>();
}
