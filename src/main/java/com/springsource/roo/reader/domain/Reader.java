package com.springsource.roo.reader.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Reader {

    @NotNull
    @Size(max = 10)
    private String username;

    @NotNull
    @Size(min = 8)
    private String password;

    @NotNull
    @Size(max = 50)
    private String firstname;

    @NotNull
    @Size(max = 50)
    private String lastname;

    @NotNull
    @Size(max = 80)
    private String email;

    @Size(max = 10)
    private String streetno;

    @Size(max = 50)
    private String streetname;

    @Size(max = 30)
    private String surburb;

    @Size(max = 30)
    private String states;

    @NotNull
    private Integer postcode;

    @NotNull
    @Size(max = 500)
    private String interests;

    @ManyToOne
    private UserRole userRole;

    @NotNull
    private Boolean activestatus;

    @NotNull
    private Boolean subscribe;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Rating> rates = new HashSet<Rating>();

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Topic> topics = new HashSet<Topic>();

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Readinglist> readinglists = new HashSet<Readinglist>();
}
