// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.roo.reader.domain;

import com.springsource.roo.reader.domain.Comment;
import com.springsource.roo.reader.domain.Reader;
import com.springsource.roo.reader.domain.Topic;
import java.util.Date;

privileged aspect Comment_Roo_JavaBean {
    
    public Reader Comment.getPostby() {
        return this.postby;
    }
    
    public void Comment.setPostby(Reader postby) {
        this.postby = postby;
    }
    
    public Date Comment.getCommentdate() {
        return this.commentdate;
    }
    
    public void Comment.setCommentdate(Date commentdate) {
        this.commentdate = commentdate;
    }
    
    public String Comment.getContent() {
        return this.content;
    }
    
    public void Comment.setContent(String content) {
        this.content = content;
    }
    
    public Integer Comment.getThumbup() {
        return this.thumbup;
    }
    
    public void Comment.setThumbup(Integer thumbup) {
        this.thumbup = thumbup;
    }
    
    public Topic Comment.getTopic() {
        return this.topic;
    }
    
    public void Comment.setTopic(Topic topic) {
        this.topic = topic;
    }
    
}
