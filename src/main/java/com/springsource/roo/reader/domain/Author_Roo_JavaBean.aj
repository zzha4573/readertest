// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.roo.reader.domain;

import com.springsource.roo.reader.domain.Author;
import com.springsource.roo.reader.domain.Country;

privileged aspect Author_Roo_JavaBean {
    
    public String Author.getFirstName() {
        return this.firstName;
    }
    
    public void Author.setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String Author.getLastName() {
        return this.lastName;
    }
    
    public void Author.setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public Country Author.getCountry() {
        return this.country;
    }
    
    public void Author.setCountry(Country country) {
        this.country = country;
    }
    
}
