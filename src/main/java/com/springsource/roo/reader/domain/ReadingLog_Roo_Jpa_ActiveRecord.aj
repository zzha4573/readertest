// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.roo.reader.domain;

import com.springsource.roo.reader.domain.ReadingLog;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect ReadingLog_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager ReadingLog.entityManager;
    
    public static final EntityManager ReadingLog.entityManager() {
        EntityManager em = new ReadingLog().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long ReadingLog.countReadingLogs() {
        return entityManager().createQuery("SELECT COUNT(o) FROM ReadingLog o", Long.class).getSingleResult();
    }
    
    public static List<ReadingLog> ReadingLog.findAllReadingLogs() {
        return entityManager().createQuery("SELECT o FROM ReadingLog o", ReadingLog.class).getResultList();
    }
    
    public static ReadingLog ReadingLog.findReadingLog(Long id) {
        if (id == null) return null;
        return entityManager().find(ReadingLog.class, id);
    }
    
    public static List<ReadingLog> ReadingLog.findReadingLogEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM ReadingLog o", ReadingLog.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void ReadingLog.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void ReadingLog.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            ReadingLog attached = ReadingLog.findReadingLog(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void ReadingLog.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void ReadingLog.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public ReadingLog ReadingLog.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        ReadingLog merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
