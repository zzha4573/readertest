// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.roo.reader.domain;

import com.springsource.roo.reader.domain.Book;
import com.springsource.roo.reader.domain.Rating;
import com.springsource.roo.reader.domain.RatingScale;
import com.springsource.roo.reader.domain.Reader;

privileged aspect Rating_Roo_JavaBean {
    
    public Book Rating.getBook() {
        return this.book;
    }
    
    public void Rating.setBook(Book book) {
        this.book = book;
    }
    
    public Reader Rating.getReader() {
        return this.reader;
    }
    
    public void Rating.setReader(Reader reader) {
        this.reader = reader;
    }
    
    public RatingScale Rating.getRatingscale() {
        return this.ratingscale;
    }
    
    public void Rating.setRatingscale(RatingScale ratingscale) {
        this.ratingscale = ratingscale;
    }
    
}
