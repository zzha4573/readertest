package com.springsource.roo.reader.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class UserRole {

    @NotNull
    @Size(min = 2)
    private String RoleName;

    @NotNull
    @Size(min = 2, max = 500)
    private String Description;
}
