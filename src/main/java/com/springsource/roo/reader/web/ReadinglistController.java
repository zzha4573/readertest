package com.springsource.roo.reader.web;

import com.springsource.roo.reader.domain.Readinglist;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/readinglists")
@Controller
@RooWebScaffold(path = "readinglists", formBackingObject = Readinglist.class)
public class ReadinglistController {
}
