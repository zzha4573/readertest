package com.springsource.roo.reader.web;

import com.springsource.roo.reader.domain.RatingScale;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/ratingscales")
@Controller
@RooWebScaffold(path = "ratingscales", formBackingObject = RatingScale.class)
public class RatingScaleController {
}
