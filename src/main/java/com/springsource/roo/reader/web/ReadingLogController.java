package com.springsource.roo.reader.web;

import com.springsource.roo.reader.domain.ReadingLog;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/readinglogs")
@Controller
@RooWebScaffold(path = "readinglogs", formBackingObject = ReadingLog.class)
public class ReadingLogController {
}
