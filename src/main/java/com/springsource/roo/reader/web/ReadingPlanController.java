package com.springsource.roo.reader.web;

import com.springsource.roo.reader.domain.ReadingPlan;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/readingplans")
@Controller
@RooWebScaffold(path = "readingplans", formBackingObject = ReadingPlan.class)
public class ReadingPlanController {
}
