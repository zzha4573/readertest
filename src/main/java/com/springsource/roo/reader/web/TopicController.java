package com.springsource.roo.reader.web;

import com.springsource.roo.reader.domain.Topic;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/topics")
@Controller
@RooWebScaffold(path = "topics", formBackingObject = Topic.class)
public class TopicController {
}
