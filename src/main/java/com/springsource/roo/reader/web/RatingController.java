package com.springsource.roo.reader.web;

import com.springsource.roo.reader.domain.Rating;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/ratings")
@Controller
@RooWebScaffold(path = "ratings", formBackingObject = Rating.class)
public class RatingController {
}
