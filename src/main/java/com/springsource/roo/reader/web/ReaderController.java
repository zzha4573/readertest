package com.springsource.roo.reader.web;

import com.springsource.roo.reader.domain.Reader;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/readers")
@Controller
@RooWebScaffold(path = "readers", formBackingObject = Reader.class)
public class ReaderController {
}
