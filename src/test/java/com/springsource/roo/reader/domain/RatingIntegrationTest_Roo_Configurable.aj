// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.roo.reader.domain;

import com.springsource.roo.reader.domain.RatingIntegrationTest;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect RatingIntegrationTest_Roo_Configurable {
    
    declare @type: RatingIntegrationTest: @Configurable;
    
}
