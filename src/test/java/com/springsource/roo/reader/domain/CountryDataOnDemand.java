package com.springsource.roo.reader.domain;

import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = Country.class)
public class CountryDataOnDemand {
}
