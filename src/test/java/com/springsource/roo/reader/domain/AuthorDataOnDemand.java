package com.springsource.roo.reader.domain;

import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = Author.class)
public class AuthorDataOnDemand {
}
