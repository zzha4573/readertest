// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.springsource.roo.reader.domain;

import com.springsource.roo.reader.domain.Readinglist;
import com.springsource.roo.reader.domain.ReadinglistDataOnDemand;
import com.springsource.roo.reader.domain.ReadinglistIntegrationTest;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

privileged aspect ReadinglistIntegrationTest_Roo_IntegrationTest {
    
    declare @type: ReadinglistIntegrationTest: @RunWith(SpringJUnit4ClassRunner.class);
    
    declare @type: ReadinglistIntegrationTest: @ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext*.xml");
    
    declare @type: ReadinglistIntegrationTest: @Transactional;
    
    @Autowired
    private ReadinglistDataOnDemand ReadinglistIntegrationTest.dod;
    
    @Test
    public void ReadinglistIntegrationTest.testCountReadinglists() {
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", dod.getRandomReadinglist());
        long count = Readinglist.countReadinglists();
        Assert.assertTrue("Counter for 'Readinglist' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void ReadinglistIntegrationTest.testFindReadinglist() {
        Readinglist obj = dod.getRandomReadinglist();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to provide an identifier", id);
        obj = Readinglist.findReadinglist(id);
        Assert.assertNotNull("Find method for 'Readinglist' illegally returned null for id '" + id + "'", obj);
        Assert.assertEquals("Find method for 'Readinglist' returned the incorrect identifier", id, obj.getId());
    }
    
    @Test
    public void ReadinglistIntegrationTest.testFindAllReadinglists() {
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", dod.getRandomReadinglist());
        long count = Readinglist.countReadinglists();
        Assert.assertTrue("Too expensive to perform a find all test for 'Readinglist', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<Readinglist> result = Readinglist.findAllReadinglists();
        Assert.assertNotNull("Find all method for 'Readinglist' illegally returned null", result);
        Assert.assertTrue("Find all method for 'Readinglist' failed to return any data", result.size() > 0);
    }
    
    @Test
    public void ReadinglistIntegrationTest.testFindReadinglistEntries() {
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", dod.getRandomReadinglist());
        long count = Readinglist.countReadinglists();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<Readinglist> result = Readinglist.findReadinglistEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'Readinglist' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'Readinglist' returned an incorrect number of entries", count, result.size());
    }
    
    @Test
    public void ReadinglistIntegrationTest.testFlush() {
        Readinglist obj = dod.getRandomReadinglist();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to provide an identifier", id);
        obj = Readinglist.findReadinglist(id);
        Assert.assertNotNull("Find method for 'Readinglist' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifyReadinglist(obj);
        Integer currentVersion = obj.getVersion();
        obj.flush();
        Assert.assertTrue("Version for 'Readinglist' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void ReadinglistIntegrationTest.testMergeUpdate() {
        Readinglist obj = dod.getRandomReadinglist();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to provide an identifier", id);
        obj = Readinglist.findReadinglist(id);
        boolean modified =  dod.modifyReadinglist(obj);
        Integer currentVersion = obj.getVersion();
        Readinglist merged = obj.merge();
        obj.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'Readinglist' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void ReadinglistIntegrationTest.testPersist() {
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", dod.getRandomReadinglist());
        Readinglist obj = dod.getNewTransientReadinglist(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'Readinglist' identifier to be null", obj.getId());
        obj.persist();
        obj.flush();
        Assert.assertNotNull("Expected 'Readinglist' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void ReadinglistIntegrationTest.testRemove() {
        Readinglist obj = dod.getRandomReadinglist();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Readinglist' failed to provide an identifier", id);
        obj = Readinglist.findReadinglist(id);
        obj.remove();
        obj.flush();
        Assert.assertNull("Failed to remove 'Readinglist' with identifier '" + id + "'", Readinglist.findReadinglist(id));
    }
    
}
