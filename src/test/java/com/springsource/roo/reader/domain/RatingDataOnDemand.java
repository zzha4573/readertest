package com.springsource.roo.reader.domain;

import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = Rating.class)
public class RatingDataOnDemand {
}
